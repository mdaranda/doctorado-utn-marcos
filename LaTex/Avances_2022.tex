\documentclass[a4paper,12pt]{article}
% Paquetes

\usepackage[utf8]{inputenc}
\usepackage[spanish,es-tabla]{babel}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{chicago}
\usepackage{listings}

\title{YOLO - Mask R-CNN}
\author{Marcos D. Aranda}

\begin{document}
\maketitle 
\textbf{Aprendizaje Automático\\} 
\\
En la detección de objetos se suelen emplear técnicas de Aprendizaje Automático o Machine Learning, esta área consiste en crear sistemas que pueden aprender por sí mismos, capaces de identificar una serie de patrones complejos a partir de datos de entrada. Este proceso se realiza mediante algún algoritmo de aprendizaje \cite{abu2012learning}. Existen varios tipos de aprendizaje automático:

\begin{itemize}

	\item Aprendizaje supervisado: Se trata de un tipo de aprendizaje donde al sistema se le alimenta con datos de entrenamiento etiquetados. Una vez se ha completado el entrenamiento, se pueden introducir nuevos datos al sistema de manera que ya no están etiquetados, puesto que al estar entrenado el sistema es capaz de reconocer patrones para identificar los datos correspondientes. En este tipo de aprendizaje están los denominados problemas de clasificación y los de regresión \cite{abu2012learning}.
	\item Aprendizaje no supervisado: En este tipo de aprendizaje se alimenta el sistema sin datos etiquetados. Uno de los problemas más comunes de este tipo de aprendizaje automático es el de agrupamiento o clustering, en el que el sistema aprende a organizar los datos en distintos grupos, de modo que cada grupo contenga datos similares entre sí \cite{abu2012learning} .
	
\end{itemize}

\textbf{Aprendizaje Profundo\\}

El aprendizaje profundo o deep learning es un campo dentro del área del Machine Learning. Se trata de un paradigma de aprendizaje automático que intenta asemejarse a la forma del aprendizaje humano para poder aprender determinados datos \cite{geron2019hands}. Para el procesamiento de la información, utiliza redes neuronales artificiales, pero concatenando un gran número de capas formadas por unidades de procesamiento sencillas (neuronas), que van transformando progresivamente la información \cite{rozada2021estudio}\\.

\textbf{Detección de objetos\\}

Normalmente los problemas más “habituales” dentro de machine learning, son los llamados problemas de clasificación. Se trata de que a partir de unos datos concretos y en función de los parámetros con los que se configure el sistema, éste sea capaz de clasificarlos de una manera adecuada, en función siempre de una serie de variables \cite{rozada2021estudio}.\\

Sin embargo, hay un problema de machine learning que va un paso más allá, que es la detección de objetos. Se trata de un problema más complejo que el de la clasificación, ya que consiste en no solo decir si el objeto está o no presente en la imagen en cuestión, sino también en qué región de la imagen se encuentra dicho objeto \cite{rozada2021estudio}.  El eje central de un detector de objetos basado en deep learning es la red neuronal, que será la encargada de realizar las operaciones necesarias para extraer las características de las imágenes y así poder obtener las características que se corresponden con un objeto, todo con el objetivo de que pueda aprender a reconocer el objeto, además dónde está dicho objeto concretamente en la imagen.\\

\textbf{Red neuronal\\}

Para poder entender un red neuronal, es necesario explicar lo que es una TLU (threshold logic unit) o unidad lógica de umbral. Se trata de una neurona artificial, que se compone de una serie de entradas y de una salida. Las entradas están asociadas a unos pesos de forma que la TLU calcula la suma ponderada de sus entradas para obtener una salida (ver Figura 1)\cite{geron2019introduction}.\\
\begin{figure}[h] % opción de posicionamiento
    \centering % imagen centrada
    \includegraphics[width=0.75\textwidth]{/home/marcos/Documentos/Doctorado - UTN/Imagenes/TLU.png}
    \caption{Estructura de un TLU o unidad lógica de umbral}
\end{figure}

Un perceptrón, es una capa de TLUs con cada TLU conectada a todas las entradas. Cada TLU se activará en función de si la suma ponderada de las entradas tiene un valor mayor o menor que un umbral. De esta forma si la suma ponderada es mayor que el umbral, tendremos una salida de valor 1, mientras que si es menor que el umbral, la salida será nula o bien -1, todo dependiendo del tipo de función de activación \cite{geron2019introduction}.. Las funciones de activación dan un valor de salida según sea el valor que tienen a la entrada. Estos valores de salida pueden ser entre 0 y 1 o bien entre -1 y 1, aunque también existen otras posibilidades \cite{rozada2021estudio}.
En una red neuronal para detección de objetos, se utilizan las redes neuronales convolucionales, ya que el problema que nos ocupa es el procesamiento de imágenes. En este caso lo que hace la red es trabajar como entradas, con los píxeles de cada imagen. En este caso, en una red convolucional, en la primera capa, no está conectada a todos los píxeles de la imagen de entrada,  sino que está conectada a una porción de ellos (ver Figura 2)\cite{geron2019introduction}.\\
\begin{figure}[h] % opción de posicionamiento
    \centering % imagen centrada
    \includegraphics[width=0.75\textwidth]{/home/marcos/Documentos/Doctorado - UTN/Imagenes/CNN.png}
    \caption{Esquema de una red neuronal convolucional}
\end{figure}

En la figura se puede observar como la primera capa convolucional está conectada únicamente al área de pixeles delimitada por los recuadros que podemos ver en la imagen de entrada, que son los pixeles de los campos receptivos. A su vez, cada neurona en la segunda capa convolucional está conectada solo a las neuronas ubicadas dentro de un pequeño rectángulo en la primera capa. De esta forma se van extrayendo las características pequeñas en la primera capa oculta, y se luego ensamblarlas en características más grandes de nivel superior en la siguiente capa oculta, y así sucesivamente. Esta estructura jerárquica es común en las imágenes del mundo real, que es una de las razones por las cuales las CNN funcionan tan bien para el reconocimiento de imágenes \cite{geron2019introduction}.\\

\textbf{Proceso de entrenamiento\\}

El entrenamiento es el proceso por el que el sistema va consumiendo los datos de entrada, para que pueda ir obteniendo de cada entrada los objetos que se desean detectar. Con ello, permitirá que el sistema aprenda el objeto que deseamos que detecte y que así se pueda utilizar para reconocimiento del mismo de manera automática sin la supervisión humana.
A continuación se enumeran los pasos para el proceso de entrenamiento:

\begin{enumerate}
\item Preparación de los datos, hay que destacar que algunas redes utilizadas en la detección de objetos tales como YOLO (You Only Look Once) y Mask R-CNN que hablaremos más adelante, funcionan con un tamaño de imagen determinado. 
\item Etiquetar las imágenes, puesto que en la detección de objetos es necesario indicar al sistema dónde está localizado el objeto en la imagen, para que este sea capaz de aprender dicho objeto. Los  programas que se utilizaron para el etiquetado en YOLO fue labelImg\footnote{Es una herramienta para etiquetar cuadros delimitadores de objetos en imágenes para YOLO.},  y para Mask R-CNN se utilizo VGG Image Annotator\footnote{Es un proyecto de código abierto desarrollado por Visual Geometry Group utilizado para etiquetar regiones en polígonos para Mask R-CNN.}, aunque estas herramientas brindan un entorno gráfico amigable este proceso lleva su tiempo. Sin embargo, una vez etiquetado, obtenemos un fichero de anotación para cada imagen, en el que estará presente cada objeto que hay en la imagen, con su respectiva clase para que e para que el sistema pueda utilizarlo a la hora del entrenamiento. 
\item Normalización del tamaño de las imágenes y de los valores de los píxeles de la imagen. En el caso de normalizar el tamaño, se hace debido a fines de mejora de la eficiencia de computación y además porque la red se adaptará mejor a un tamaño de imagen concreto en lugar de tamaños de imagen dispares.
\item Para la correcta ejecución del entrenamiento es necesario especificar una serie de parámetros para su adecuado progreso. En primer lugar, encontramos las épocas de las que consta el entrenamiento, que es el número de veces que vamos a iterar sobre todo el conjunto de datos durante el entrenamiento. Para nuestras 2 redes utilizadas, se uso 100 épocas, el entrenamiento durará el tiempo en que se tarde en recorrer el conjunto de datos de entrenamiento 100 veces. Otro parámetro importante es el número de clases, que es el número de clases de objetos distintos que vamos a entrenar.
\item Posteriormente, es necesario especificar la red neuronal que se vamos a utilizar para nuestra detección y por ende para nuestro entrenamiento. Esto se puede programar de diversas maneras, como por ejemplo declarando cada una de las capas una a una, aunque hay casos en los que se pueden cargar un modelo predeterminado de red. Algunos ejemplos son Resnet, GoogleNet, VGG, etc. Estas redes se pueden entrenar desde cero, es decir alimentando la red directamente con los datos de entrada, pero también se puede aprovechar el aprendizaje que ha realizado la red para una serie de objetos y aplicarlo al aprendizaje de un nuevo objeto que deseamos que detecte, y esto es lo que se conoce como Transfer learning \cite{geron2019introduction}. Supongamos que tenemos una red neuronal que ha aprendido a identificar una clase dada de objeto, pero creamos una nueva red neuronal con la que deseamos detectar otra clase de objeto distinta. En este punto podemos o bien entrenar inicializando aleatoriamente pesos y sesgos de las primeras capas de la nueva red neuronal, o bien podemos inicializarlos al valor de los pesos y sesgos de las capas inferiores de la primera red. De esta manera, la red no tendrá que aprender desde cero todas las estructuras de bajo nivel, evitando un tiempo de entrenamiento mayor. Con esto, la red solo tendrá que aprender las estructuras de nivel superior. La ventaja de esta técnica es que el entrenamiento es mucho más rápido, y además se requieren menos datos a la hora de entrenar\cite{geron2019introduction}. 
\item Compilación del modelo, para poder definir la función de pérdidas que nos permite evaluar cómo de bien funciona el algoritmo al consumir los datos y el optimizador que se seguirá durante el proceso. El optimizador permite ajustar los parámetros del modelo para minimizar las pérdidas de la función de coste sobre el conjunto de datos de entrenamiento.
\item Finalmente tenemos el proceso de detección de los objetos sobre la red que se ha entrenado para la detección de estos. En este caso, lo que hacemos es cargar la red neuronal con los pesos que se acaban de ajustar durante el entrenamiento, y posteriormente se alimenta la red con datos (imágenes en nuestro caso) que contienen (o no) el objeto que estamos intentando detectar. \\
\end{enumerate}
 
\textbf{YOLO (You Only Look Once)\\}

Se trata de una arquitectura de detección de objetos muy rápida y precisa, que fue creada por Joseph Redmond como principal autor, ya que la misma es una arquitectura muy rápida, la hace idónea para ser implementada en la detección de video en tiempo real. YOLO consiste en una red neuronal convolucional que predice simultáneamente múltiples cuadros delimitadores y las probabilidades de la clase de objeto que delimitan dichos cuadros delimitadores \cite{redmon2016you}.  Se puede destacar que este modelo es un sistema muy rápido, y esto hace que sea la principal alternativa para detección de objetos en tiempo real, ya que  las detecciones se realizan a partir de una única red neuronal, pudiéndose entrenar de principio a fin para mejorar la precisión.
A lo largo de los años YOLO ha ido evolucionando desde su aparición en 2015.  La  versión YOLOv2, tiene como objetivo mejorar la precisión de manera notable y hacer una detección aún más rápida. En cuanto a las mejoras de precisión, se añade la normalización por lotes en las capas de convolución \cite {kamal2019yolo} \cite{redmon2016you}. La normalización por lotes o Batch Normalization se trata de una técnica que consiste en añadir operaciones en el modelo, antes o después de la función de activación de cada capa oculta. A continuación, se normaliza cada entrada, y se realiza un centrado en cero. Finalmente se escala y se cambia el resultado usando dos nuevos vectores de parámetros por capa. Esta operación permite que el modelo aprenda la escala y la media óptimas de cada una de las entradas de la capa \cite{geron2019introduction}.\\

Para una primera prueba del proyecto de manera local se ha utilizado YOLOv3, esta versión nace con el propósito de aumentar la precisión en la detección de objetos, esto hace que se gane en robustez, y esto se debe principalmente a la arquitectura de red usada, que es Darknet-53.Esta red está compuesta por 53 capas completamente convolucionales, de ahí el nombre de Darknet-53 \cite{redmon2018yolov3}.\\

Para ejecutar el proyecto en la nube se utilizo YOLOv4, el cual es una versión donde Joseph Redmon ya no es autor, ya que al finalizar la versión 3, cesó su investigación. En esta versión, su motivo principal es optimizar el detector para realizar cálculos en paralelo. Se compone de 3 etapas que podemos observar en la Figura 3:
\begin{itemize}
\item \textbf{Backbone}: CSPDarknet53, una red que aumenta la capacidad de aprendizaje y que gracias a la unión del módulo de agrupación de pirámide espacial permite mejorar el campo receptivo y distinguir características importantes.
\item \textbf{Neck}: Módulo de agrupación de pirámides espaciales y agregación de ruta PANet, principalmente. PANet se implementa como sustitución de las redes piramidales de características empleadas para la detección en YOLOv3.
\item \textbf{Head}: YOLOv3
\end{itemize}
\begin{figure}[h] % opción de posicionamiento
    \centering % imagen centrada
    \includegraphics[width=1\textwidth]{/home/marcos/Documentos/Doctorado - UTN/Imagenes/Yolov4.png}
    \caption{Proceso de entrenamiento dataset COCO y modelo YOLOv4}
\end{figure}

\textbf{Mask R-CNN\\}

Mask R-CNN (regional convolutional neural network) es un arquitectura de dos etapas: la primera etapa escanea la imagen y genera propuestas (áreas que pueden contener un objeto) y la segunda etapa de clasificación de propuestas y genera señales de limites de las cajas y mascaras. La red neuronal Faster R-CNN es un marco popular para la detección de objetos y Mask R-CNN lo amplía con la segmentación de instancias, entre otras cosas para ampliar su predecesor.
Se compone de los siguientes componentes\cite{.....}:
\begin{itemize}
\item \textbf{Red Troncal}: Esta es una red neuronal convulsionar estándar (típicamente ResNet\footnote{Es una red neuronal artificial}50 o ResNet101) que sirve como un extractor de características. Las primeras capas detentan las características de bajo nivel y las capas posteriores detectan sucesivamente características de nivel superior.
\item \textbf{Región Propuesta de Red}: El RPN es una red neuronal liviana que escanea la imagen en forma de ventana deslizante y encuentra áreas que contienen objetos. Las regiones que escanea el RPN se denominan anclas. Que son cajas distribuidas en el
área de la imagen (ver Figura 4). En la práctica, hay alrededor de 200000 anclas de diferentes tamaños y relaciones de aspecto, y se superponen para cubrir la mayor parte posible de la imagen. La ventana deslizante es manejada por la naturaleza convolucional del RPN, lo que le permite escanear todas las regiones en paralelo utilizando una GPU (Unidad de Procesamiento Gráfico). 
\begin{figure}[h] % opción de posicionamiento
    \centering % imagen centrada
    \includegraphics[width=1\textwidth]{/home/marcos/Documentos/Doctorado - UTN/Imagenes/RPN.png}
    \caption{Región escaneada por RPN}
\end{figure}
\item \textbf{Representación de máscara}: Mask R-CNN utiliza una red totalmente conectada para predecir la máscara. Esta ConvNet toma un ROI como entrada y genera la representación de la máscara m*m . 
\item \textbf{Alineación de retorno de la inversión (ROI)}: Los clasificadores no manejan muy bien el tamaño de entrada variable. Por lo general, requieren un tamaño de entrada fijo. Pero, debido al paso de refinamiento del cuadro delimitador en el RPN, los cuadros de ROI pueden tener diferentes tamaños. Ahí es donde entra en juego el ROI Pooling.
\end{itemize}

\textbf{Desarrollo\\}

En este proyecto uno de los objetivos es poder realizar  clasificación de y detección de diferentes tipos de animales (vacas, cabras y ovejas), por lo que utilizaremos  el aprendizaje supervisado y que lo que hacemos es alimentar el sistema de machine learning mediante imágenes de distintos objetos. Cada uno de estos objetos llevará asociada al menos una etiqueta, el tipo de objeto que es. Con ello, entrenamos el sistema durante un tiempo, después del cual, podremos evaluar la inferencia, se ha realizado el entrenamiento para reconocimiento de  la clase cabra ya que el dataset de COCO no posee como objeto a esta clase, en primer instancia se utilizaron 39 imágenes para el entrenamiento y 9 para la validación para ambos modelos.\\

Para la implementación de la manera remoto, se utilizo YOLOv4  donde el tiempo de entrenamiento aproximado fue de 5 horas, logrando resultado del 80\%, se comenta que a partir del año 2021 las políticas de Google Colab incorporaron que una notebook no puede utilizar los recursos por más de 12 horas, es por ellos que para aumentar el grado de exactitud en la Inferencia se debe realizar el entrenamiento en un servidor local. Para el test se utilizaron imágenes y videos, en el caso de las primera se realizo con una exactitud de 90\% (ver Figura 5).En el segundo caso la mayor precisión fue de 85\% en video. Para obtener mejores resultados se etiqueto con el softtware labelImg 190 imágenes, pero el problema que se tuvo en Google Colab es que llevaba más de 12 horas el entrenamiento por tanto no se lograba terminar con el mismo, es por ello que se solicito colaboración al Centro de Computación de Altos Desempeños de la Universidad Nacional de Córdoba (CCAD) y brindo el acceso a un computoador remoto y así poder continuar con la investigación. \\

La computadora que se brindo es la  Nabucodonosor, la cual surge en 2017 cuando desde el CCAD se realizo una propuesta de crear una computadora que colabore  entre Academia y Empresa para potenciar el uso de Aprendizaje Automático mediante Computadoras (machine learning – ML). El hardware de esta computadora esta compuesto por 2 Xeon E5-2680v2 de 10 núcleos cada uno, 64 GiB RAM en 8 módulos de 8 GiB DDR3 1600 MT/s y 3 GPU NVIDIA GTX 1080Ti entre otras características, el software que posee la misma es un Debian Buster (amd64) [11]. Por lo tanto con estas características se logro realizar el entrenamiento para 100 épocas utilizando YOLO con un dataset de 190 imágenes, 170 para el entrenamiento y 20 para la validación, en  este caso se utilizo PyTorch\footnote{Es una biblioteca de aprendizaje automático  de código abierto basada en la biblioteca de Torch, utilizado para aplicaciones que implementan cosas como visión artificial y procesamiento de lenguajes naturales, principalmente desarrollado por el Laboratorio de Investigación de Inteligencia Artificial de Facebook (FAIR).}, posteriormente se descargo el último peso generado yolov3\_ckpt\_99\.pth ya que los mismos se generar de 0 a 99, se ejecuto YOLO en un a computadora local la cual solamente posee CPU por lo que la inferencia posee retardo sin embargo se obtuvo un grado de exactitud del 98\% en tiempo real.\\ 

\begin{figure}[h] % opción de posicionamiento
    \centering % imagen centrada
    \includegraphics[width=.65\textwidth]{/home/marcos/Documentos/Doctorado - UTN/Imagenes/cabra_yolo.jpg}
    \caption{Detección de cabras con YOLO}
\end{figure}
En el  caso de utilizar Mask R-CNN, hasta el momento solo se ha logrado implementar de manera remota a través de Google Colab, el dataset compuesto por un total de 48 imágenes se han etiquetado  con la herramienta  VGG Image Annotator, posteriormemte se realizo el entrenamiento para 10 épocas y se han  observado que el algoritmo necesita tener un mayor entrenamiento y dataset para minimizar el error, ya que el mismo esta detectado objetos que no son cabras (ver Figura 6 y 7).
\begin{figure}[h] % opción de posicionamiento
    \centering % imagen centrada
    \includegraphics[width=1\textwidth]{/home/marcos/Documentos/Doctorado - UTN/Imagenes/mask_1.png}
    \caption{Detección de cabras con Mask R-CNN - Verdadera}
\end{figure}

\begin{figure}[h] % opción de posicionamiento
    \centering % imagen centrada
    \includegraphics[width=1\textwidth]{/home/marcos/Documentos/Doctorado - UTN/Imagenes/mask_2.png}
    \caption{Detección de cabras con Mask R-CNN - Falsa}
\end{figure} 

\bibliographystyle{chicago}
\bibliography{Referencias}
\end{document}
